Mise en œuvre des exercices sous R de la leçon Monte Carlo
==========================================================


Exercice 1. 

(b) Construire une fonction permettant de calculer p (utiliser pnorm)
(d) Tracer p en fonction de K (utiliser plot ou curve)
(e) Construire un estimateur Monte Carlo (utiliser rnorm et mean)
(f) Construire une fonction retournant l'estimateur, l'intervalle de confiance et comparer à la valeur analytique donnée par (b)
(g) Tracer (1-p_N)^{1/2}/(p_N^{1/2}N) en fonction de K

Exercice 2. 

Calculer le prix d'un call et d'un put en utilisant la méthode de Monte Carlo, 
en calculant aussi les intervalles de confiance. Tracer l'erreur relative
en fasisant grandir le strike ou le prix d'exercice.

Exercice 7.

Fonctions utiles
    matrix construire une matrice
    %*% pour un produit matriciel
    quantile quantile empirique
    qnorm inverse de la fonction de répartition de la normale
